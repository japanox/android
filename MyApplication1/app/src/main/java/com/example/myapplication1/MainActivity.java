package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private String url = "https://www.naver.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true); // 자바스크립트 동작허용
        webView.loadUrl(url); // url 로드
        webView.setWebChromeClient(new WebChromeClient()); // 크롬 세팅
        webView.setWebViewClient(new WebViewClientClass());


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // 뒤로가기 만들기
        //컨트롤 + O키로 뭘열어서, onkeydown 엔터
        if((keyCode == KeyEvent.KEYCODE_BACK)&& webView.canGoBack()){ // 뒤로가기
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event); // 특정키를 입력할때 특정동작을 할대 사용
    }


    private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
