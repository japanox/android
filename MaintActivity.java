package com.example.newstart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText et_id;
    Button btn_test;
    private Button btn_move;
    private Button btn_trans;
    private String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_id =findViewById(R.id.et_id); //아이디 내용을 가져온다.
        btn_test=findViewById(R.id.btn_test);

        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_id.setText("버튼클릭완료");
            }
        });


        et_id = findViewById(R.id.et_id);


        btn_move =findViewById(R.id.btn_move); // 아이디를 가져와라
        btn_move.setOnClickListener(new View.OnClickListener() { //btn 클릭시 액션 수행
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SubActivity.class); // 액티비티 생성
                startActivity(intent); // 액티비티 이동
            }
        });

        btn_trans=findViewById(R.id.btn_trans);
        btn_trans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str = et_id.getText().toString(); // str에 et_id 텍스박스의 내용을 stirng형태로 가져온다
                Intent intent = new Intent(MainActivity.this, SubActivity.class); // 액티비티 생성
                intent.putExtra("str",str); // 서브액티비티로 전송
                startActivity(intent); // 액티비티 이동
            }
        });

    }
}
